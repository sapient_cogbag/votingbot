import discord
import asyncio as coro
import csv
import os
import os.path
import string
from typing import *
from discord.ext import commands

bot = commands.Bot('$')
DbSpec = List[Tuple[str, Optional[Callable[[Any], str]], Optional[Callable[[str], Any]]]]

def parse_to_dbspec(components: Tuple, dbspec: DbSpec)  -> Dict[str, Any]:
    """
    Simply takes a tuple and parses it with a dbspec
    i.e. a list of tuples containing "name", optional converter to string, optional converter from string 
    """
    res = {}
    for i in range(min(len(components), len(dbspec))):
        conv = dbspec[i][2] if dbspec[i][2] is not None else lambda x: x
        res[dbspec[i][0]] = conv(components[i]) 
    
    return res

def parse_from_dbspec(components: Dict, dbspec: DbSpec) -> Tuple:
    """
    Convert a data element back into a tuple of strings (the row)
    """
    res = []
    for delement in dbspec:
        name = delement[0]
        value = components[name]
        invert_conv = delement[1] if delement[1] is not None else str
        res.append(invert_conv(value))

    return tuple(res)

def is_valid_category(category: str) -> bool:
    return set(string.ascii_letters + string.digits + '-').issuperset(set(category))

def reduce_category(category_specifier: str) -> str:
    """
    Reduce a string into a standard category format
    """
    return category_specifier.strip().lower()

def cat_file_base(category: str) -> Optional[str]:
    """
    Gets the filepath base for the given category.
    Categories can only contain alphanumeric characters and dashes.
    Also creates the db directory if not existing.

    Returns None for invalid categories
    """
    if not category_valid:
        return None
    else:
        if not os.path.exists("db"):
            os.makedirs("db", exist_ok=True)
        return os.path.join("db", category)


def cat_file_vote_db(category: str) -> Optional[str]:
    """
    gets the filepath for the votedb of the given category
    """
    category_base = cat_file_base(category)
    
    res = None if category_base is None else category_base + "-votedb.csv"
    if res is not None:
        if not os.path.exists(res):
            with open(res, "wt") as empty_file:
                pass
    return res

def cat_file_candidate_list(category: str) -> Optional[str]:
    """
    Get the file name for the candidate list for the given path.
    """
    category_base = cat_file_base(category)
    res = None if category_base is None else category_base + "-candidatelist.txt"
    if res is not None:
        if not os.path.exists(res):
            with open(res, "wt") as empty_file:
                pass
    return res

def load_categories() -> Set[str]:
    """
    Load categories. Error if category file does not exist
    """
    categories = None
    try:
        with open("categories", "rt") as catf:
            categories = catf.readlines()
    except Exception:
        raise Exception("Could not open 'categories' file")

    categories = set((reduce_category(x) for x in categories if reduce_category(x) != '' and is_valid_category(x)))
    return categories


def load_candidate_ids(category: str) -> Set[int]:
    """
    Load candidate discord ids ~ any invalid rows get ignored.
    """
    discordids = None
    candidate_file_name = cat_file_candidate_list(category)
    try:
        with open(candidate_file_name, "rt") as cfn:
            discordids = set()
            for row in cfn.readlines():
                id_ = None
                try:
                    id_ = int(row.strip())
                except Exception:
                    pass
                if id_ is not None:
                    discordids.add(id_)

    except Exception:
        raise Exception("Unable to open candidates file")

def set_candidate_ids(category: str, ids: Set[int]):
    """
    Save candidate ids for the given category
    """
    candidate_file_name = cat_file_candidate_list(category)
    try:
        with open(candidate_file_name, "wt") as cfn:
            cfn.writelines((str(id_) for id_ in ids))
    except Exception:
        raise Exception("Cannot update candidates file")


def load_discord_token() -> str:
    """
    Get the discord bot token
    """
    fname = "discord_token.txt"
    try:
        with open(fname, "rt") as f:
            return f.read().strip()
    except Exception:
        raise Exception("could not load token from 'discord_token.txt'")

votedb_spec = [
    ("voter_user_id", None, int) , 
    ("candidate_user_id", None, int),
    ("rating", None, float)
]  # DbSpec for voters.


"""
A database mapping candidate discord user ids to maps of voter user ids to their rating
of the given candidate
"""
RatingDatabase = Dict[int, Dict[int, float]]

def load_votedb(vdb_file: file, valid_candidates: Set[int]) -> RatingDatabase:
    """
    read a votedb from a vote db file.
    this builds *things* nyaaa
    like a dict mapping a candidate user
    id to a dict mapping voter user ids
    to rating.
    
    vdb_file must be a text readable file opened with `newline=''`, as specified
    in https://docs.python.org/3.8/library/csv.html
    """
    res = {}

    for row in csv.reader(vdb_file):
        mapped_data = parse_to_dbspec(tuple(row), votedb_spec)

        voter_uid = mapped_data["voter_user_id"]
        candidate_uid = mapped_data["candidate_user_id"]
        rating = mapped_data["rating"]

        if candidate_uid in valid_candidates:
            if candidate_uid not in res:
                res[candidate_uid] = {}
            res[candidate_uid][voter_uid] = rating

    return res

def save_votedb(vdb_file: file, valid_candidates: Set[int], new_rating_db: RatingDatabase):
    """
    Resave the given voting database into the given voting CSV file.

    The file should be opened in write mode with `newline=''`, as specified by
    https://docs.python.org/3.8/library/csv.html#id3
    """
    vdb_writer = csv.writer(vdb_file)
    for candidate_id, candidate_map in new_rating_db.items():
        if candidate_id in valid_candidates:
            for voter_id, rating in candidate_map.items():
                dbentry = {
                    "voter_user_id": voter_id,
                    "candidate_user_id": candidate_id,
                    "rating": rating
                }

                row = parse_from_dbspec(dbentry, votedb_spec)
                vdb_writer.writerow(row)


def load_category_votedb(category: str, valid_candidates: Set[int]) -> RatingDatabase:
    """
    Load the vote database for the given election category.
    """
    category_path = cat_file_vote_db(category)
    try:
        with open(category_path, 'rt', newline='') as dbfile:
            return load_votedb(dbfile, valid_candidates)
    except Exception:
        raise Exception("Could not load voting database for category " + category)

    
def save_category_votedb(category: str, valid_candidates: Set[int], new_voting_db: RatingDatabase):
    """
    Save the updated voting database for the given category :)
    """
    category_path = cat_file_vote_db(category)
    try:
        with open(category_path, 'wt', newline='') as dbfile:
            save_votedb(dbfile, valid_candidates, new_voting_db)
    except Exception:
        raise Exception("Could not update voting database for category " + category)


@bot.event
async def on_ready():
    """
    Initialises the bot!

    bot.categories = set of the categories.

    bot.voting_dbs = map of categories to RatingDatabase
    bot.candidates = map of categories to a set of candidate discord user ids.
    """
    bot.categories = load_categories()

    # category-mapped stuff
    bot.voting_dbs = {}
    bot.candidates = {}
    
    # Initialise voting databases
    # and all the other stuff.
    for cat in bot.categories:
        # load candidates
        bot.candidates[cat] = load_candidate_ids(cat)

        # load voting databases
        bot.voting_dbs[cat] = load_category_votedb(cat, bot.candidates[cat])





@bot.command()
def rate_candidate(ctx: commands.Context, category: str, candidate: discord.User, rating: float):
    """
    Rate the given candidate as the given rating in the given election category.
    """

    
    

def main():
    pass

if __name__ == "__main__":
    main()
